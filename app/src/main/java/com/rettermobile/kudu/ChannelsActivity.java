package com.rettermobile.kudu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;


public class ChannelsActivity extends KuduActivity {

    private GridView mChannelsGridView;
    private ChannelsAdapter mChannelsAdapter;
    private Firebase channelsRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channels);



        this.mChannelsGridView = (GridView) findViewById(R.id.channels_grid);
        this.mChannelsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, Object> channel = (HashMap<String, Object>) ChannelsActivity.this.mChannelsAdapter.getItem(position);
                Intent intent = new Intent(ChannelsActivity.this, ContentActivity.class);
                intent.putExtra("url", (String) channel.get("url"));
                intent.putExtra("title", (String) channel.get("name"));
                ChannelsActivity.this.startActivity(intent);
            }
        });

        this.mChannelsGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final HashMap<String, Object> channel = (HashMap<String, Object>) ChannelsActivity.this.mChannelsAdapter.getItem(position);

                new AlertDialog.Builder(ChannelsActivity.this)
                        .setTitle(null)
                        .setMessage(getString(R.string.confirm_remove))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                Firebase appRef = new Firebase(getString(R.string.firebase_url) + "/apps/" + deviceId + "/installed/" + (String) channel.get("id"));
                                appRef.setValue(null);
                                String toast = String.format(getString(R.string.item_removed), (String) channel.get("name"));
                                Toast.makeText(ChannelsActivity.this, toast, Toast.LENGTH_SHORT).show();

                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                return true;
            }
        });


        String deviceId = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.key_device_id), "");
        if (deviceId.equals("")) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
        }
    }

    private class ChannelsChangedListener implements ValueEventListener {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.i("kudu", "ChannelsChangedListener onDataChange");
            ChannelsActivity.this.mChannelsAdapter = new ChannelsAdapter(ChannelsActivity.this, (HashMap<String, Object>) dataSnapshot.getValue());
            ChannelsActivity.this.mChannelsGridView.setAdapter(ChannelsActivity.this.mChannelsAdapter);


        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    private ChannelsChangedListener mChannelsChangedListener;


    @Override
    protected void onDeviceIdChanged(String deviceId) {
        super.onDeviceIdChanged(deviceId);




    }

    @Override
    protected void onResume() {
        super.onResume();

        if (this.deviceId.equals("") == false) {
            if (this.channelsRef != null && this.mChannelsChangedListener != null) {
                this.channelsRef.removeEventListener(this.mChannelsChangedListener);
                this.mChannelsChangedListener = null;
                this.channelsRef = null;
            }

            this.channelsRef = new Firebase(getString(R.string.firebase_url) + "/apps/" + deviceId + "/installed");
            this.mChannelsChangedListener = new ChannelsChangedListener();
            this.channelsRef.addValueEventListener(this.mChannelsChangedListener);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();

        if (this.channelsRef != null && this.mChannelsChangedListener != null) {
            this.channelsRef.removeEventListener(this.mChannelsChangedListener);
            this.mChannelsChangedListener = null;
            this.channelsRef = null;
        }
    }

    public class ChannelsAdapter implements ListAdapter {

        private HashMap<String, Object> mChannels;
        private Context mContext;

        public ChannelsAdapter(Context context, HashMap<String, Object> channels) {
            this.mContext = context;
            this.mChannels = channels;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            if (mChannels == null) return 0;
            return mChannels.size();
        }

        @Override
        public Object getItem(int position) {
            return this.mChannels.values().toArray()[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        private class ChannelListItemViewHolder {
            public ImageView channel_image;
            public TextView channel_name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ChannelListItemViewHolder viewHolder = null;

            if (convertView != null) {
                viewHolder = (ChannelListItemViewHolder) convertView.getTag();
            } else {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(R.layout.channel_list_item, parent, false);
                viewHolder = new ChannelListItemViewHolder();
                viewHolder.channel_name = (TextView) convertView.findViewById(R.id.channel_name);
                viewHolder.channel_image = (ImageView) convertView.findViewById(R.id.channel_image);
                convertView.setTag(viewHolder);
            }

            HashMap<String, Object> item = (HashMap<String, Object>) getItem(position);

            viewHolder.channel_name.setText((String) item.get("name"));
            Picasso.with(mContext).load((String) item.get("icon")).into(viewHolder.channel_image);

            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }
    }


}
