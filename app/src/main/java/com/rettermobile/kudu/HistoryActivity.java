package com.rettermobile.kudu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HistoryActivity extends KuduActivity implements ValueEventListener, ChildEventListener {

    private ListView mListView;
    private HistoryItemsAdapter mAdapter;
    private ArrayList<Map<String, Object>> mHistoryItems;
    private Query mHistoryRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);



    }


    @Override
    protected void onResume() {
        super.onResume();

        this.mListView = (ListView) findViewById(R.id.listViewHistory);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, Object> historyItem = (Map<String, Object>) mAdapter.getItem(position);
                Intent intent = new Intent(HistoryActivity.this, ContentActivity.class);
                intent.putExtra("url", (String) historyItem.get("containerUrl"));
                intent.putExtra("title", (String) historyItem.get("title"));
                HistoryActivity.this.startActivity(intent);
            }
        });
        if(mHistoryRef != null) {
            mHistoryRef.removeEventListener((ChildEventListener)this);
            mHistoryRef.removeEventListener((ValueEventListener)this);
        }

        mHistoryItems = new ArrayList<>();

        Query historyRef = new Firebase(getString(R.string.firebase_url) + "/history/" + this.deviceId).orderByChild("createDate").limitToLast(50);
        historyRef.addValueEventListener(this);
        historyRef.addChildEventListener(this);
    }

    // Firebase valueEventListener
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot.getValue() != null) {
            mAdapter = new HistoryItemsAdapter(this, mHistoryItems);
            mListView.setAdapter(mAdapter);
        }
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        if(dataSnapshot.getValue() != null) {
            Map<String, Object> val = (Map<String, Object>) dataSnapshot.getValue();
            mHistoryItems.add(0, val);
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {

    }

    public class HistoryItemsAdapter implements ListAdapter {

        private Context mContext;
        private ArrayList<Map<String, Object>> mData;

        public HistoryItemsAdapter(Context context, ArrayList<Map<String, Object>> data) {
            mData = data;
            mContext = context;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return mData==null ? 0 : mData.size();
        }

        @Override
        public Object getItem(int position) {
            return mData==null ? null : mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }


        private class HistoryListItemViewHolder {
            public ImageView icon;
            public TextView title;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            HistoryListItemViewHolder viewHolder = null;

            if (convertView != null) {
                viewHolder = (HistoryListItemViewHolder) convertView.getTag();
            } else {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(R.layout.history_list_item, parent, false);
                viewHolder = new HistoryListItemViewHolder();
                viewHolder.icon = (ImageView) convertView.findViewById(R.id.history_item_icon);
                viewHolder.title = (TextView) convertView.findViewById(R.id.history_item_title);
                convertView.setTag(viewHolder);
            }

            HashMap<String, Object> item = (HashMap<String, Object>) getItem(position);

            viewHolder.title.setText((String) item.get("title"));

            Picasso.with(mContext).load((String) item.get("icon")).into(viewHolder.icon);

            return convertView;

        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

}
