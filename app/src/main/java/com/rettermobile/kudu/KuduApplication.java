package com.rettermobile.kudu;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ViewConfiguration;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.lang.reflect.Field;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 * Created by baranbaygan on 30/04/15.
 */
public class KuduApplication extends Application {


    private Timer mCloseConnectionTimer;


    @Override
    public void onCreate() {
        super.onCreate();

        Firebase.setAndroidContext(this);

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }

        String remoteId = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.key_remote_id), "");
        if(remoteId.equals("")) {
            // Create a remote ID
            UUID uniqueKey = UUID.randomUUID();
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putString(getString(R.string.key_remote_id), uniqueKey.toString());
            editor.commit();
        }



        Firebase connectedRef = new Firebase("https://moviepi.firebaseio.com/.info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    Log.i("kudu", "CONNECTED");
                } else {
                    Log.i("kudu", "NOT CONNECTED");
                }
            }

            @Override
            public void onCancelled(FirebaseError error) {
                System.err.println("Listener was cancelled");
            }
        });


        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                //Log.i("kudu", "onActivityCreated");
            }

            @Override
            public void onActivityStarted(Activity activity) {
                //Log.i("kudu", "onActivityStarted");
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Log.i("kudu", "onActivityResumed");
                if(mCloseConnectionTimer != null) {
                    mCloseConnectionTimer.cancel();
                    mCloseConnectionTimer.purge();
                }

                Firebase.goOnline();

            }

            @Override
            public void onActivityPaused(Activity activity) {
                Log.i("kudu", "onActivityPaused");

                if(mCloseConnectionTimer != null) {
                    mCloseConnectionTimer.cancel();
                    mCloseConnectionTimer.purge();
                }
                mCloseConnectionTimer = new Timer();
                mCloseConnectionTimer.schedule(new TurnOffFirebaseTask(), 5000);
            }

            @Override
            public void onActivityStopped(Activity activity) {
                //Log.i("kudu", "onActivityStopped");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                //Log.i("kudu", "onActivitySaveInstanceState");
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                //Log.i("kudu", "onActivityDestroyed");
            }
        });





    }


    class TurnOffFirebaseTask extends TimerTask {

        @Override
        public void run() {
            Log.i("kudu", "TURN OFF CONNECTION");
            Firebase.goOffline();
        }
    }




}
