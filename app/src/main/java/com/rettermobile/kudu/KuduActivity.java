package com.rettermobile.kudu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by baranbaygan on 30/04/15.
 */
public class KuduActivity extends ActionBarActivity {


    protected String deviceId = "";
    protected MenuItem nowPlayingMenuItem;
    private Firebase nowplayingMediaRef;
    private ValueEventListener mNowPlayingListener;


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(nowPlayingMenuItem != null)
            nowPlayingMenuItem.setVisible(false);

        String deviceId = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.key_device_id), "");

        if (deviceId.equals(this.deviceId) == false) {
            onDeviceIdChanged(deviceId);
        }


        if (deviceId.equals("") == false) {
            PackageInfo pInfo = null;
            String version = "1.0";
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                version = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


            String remoteId = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.key_remote_id), "");
            Firebase remoteRef = new Firebase(getString(R.string.firebase_url) + "/remote/" + deviceId + "/" + remoteId);
            Map<String, String> remote = new HashMap<>();
            remote.put("os", "Android");
            remote.put("version", version);
            remoteRef.setValue(remote);
            remoteRef.onDisconnect().removeValue();
        }

        listenNowPlayingMedia();

        //SharedPreferences prefs = getPreferences()
    }

    @Override
    protected void onPause() {
        super.onPause();

        removeFirebaseEventListeners();
    }


    private void addFirebaseEventListeners() {
        listenNowPlayingMedia();
    }

    private void removeFirebaseEventListeners() {
        if(this.nowplayingMediaRef != null && this.mNowPlayingListener != null) {
            this.nowplayingMediaRef.removeEventListener(mNowPlayingListener);
            this.nowplayingMediaRef = null;
        }
    }

    protected void listenNowPlayingMedia() {
        if (deviceId.equals("") != false) return;
        if(this.nowPlayingMenuItem == null) return;
        if(this.nowplayingMediaRef != null) return;

        this.nowplayingMediaRef = new Firebase(getString(R.string.firebase_url) + "/nowplayingmedia/" + this.deviceId);
        mNowPlayingListener = this.nowplayingMediaRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("kudu", "listenNowPlayingMedia onDataChange");
                if (nowPlayingMenuItem == null) return;
                if (dataSnapshot.getValue() == null) {
                    nowPlayingMenuItem.setVisible(false);
                } else {
                    nowPlayingMenuItem.setVisible(true);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.nowplayingMediaRef = null;
    }

    protected void onDeviceIdChanged(String deviceId) {
        this.deviceId = deviceId;
    }




    protected Boolean hasMenu() {
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (this.hasMenu() == false) return true;

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);

        nowPlayingMenuItem = menu.findItem(R.id.now_playing);
        nowPlayingMenuItem.setVisible(false);

        listenNowPlayingMedia();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        } else if (id == R.id.now_playing) {
            Intent i = new Intent(this, NowPlayingActivity.class);
            startActivity(i);
            return true;
        } else if(id == R.id.action_downloads) {
            Intent i = new Intent(this, DownloadsActivity.class);
            startActivity(i);
            return true;
        } else if(id == R.id.action_history) {
            Intent i = new Intent(this, HistoryActivity.class);
            startActivity(i);
            return true;
        } else if (id == R.id.action_contact) {

            new AlertDialog.Builder(this)
                    .setTitle(null)
                    .setMessage(getString(R.string.contact_alert))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(R.string.contact_continue, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"contact@kudutime.com"});
                            i.putExtra(Intent.EXTRA_SUBJECT, "");
                            i.putExtra(Intent.EXTRA_TEXT   , "\n\nMy current RemoteID: " + deviceId);
                            try {
                                startActivity(Intent.createChooser(i, getString(R.string.send_mail)));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(KuduActivity.this, getString(R.string.no_email_clients), Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null).show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openContentActivity(String url, String title) {
        Intent i = new Intent(this, ContentActivity.class);
        i.putExtra("url", url);
        i.putExtra("title", title);
        startActivity(i);
    }


    protected Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

}
