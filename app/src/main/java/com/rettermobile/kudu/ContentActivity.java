package com.rettermobile.kudu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class ContentActivity extends KuduActivity {

    private WebView mWebView;
    private final OkHttpClient client = new OkHttpClient();
    private Boolean isSetup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);


    }

    @Override
    protected void onResume() {
        super.onResume();

        setup();
    }

    private void setup() {

        if(isSetup) return;

        isSetup = true;

        this.mWebView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        mWebView.addJavascriptInterface(new JsObject(this), "moviepiJsObject");

        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                Log.d("boxe", message + " -- From line "
                        + lineNumber + " of "
                        + sourceID);
            }
        });

        String title = getIntent().getStringExtra("title");
        String url = getIntent().getStringExtra("url");

        this.setTitle(title);

        //url = "http://192.168.1.106:7676/#/home";

        if(url.contains("deviceId")==false) {
            if (url.contains("?")) {
                url += "&deviceId=" + this.deviceId;
            } else {
                url += "?deviceId=" + this.deviceId;
            }
        }
        Log.i("kudu", "loading url: " + url);
        this.mWebView.loadUrl(url);
    }

    class JsObject {

        private Context mContext;

        public JsObject(Context context) {
            this.mContext = context;
        }

        @JavascriptInterface
        public void onHtmlCommand(String opts) {

            try {
                JSONObject jObject = new JSONObject(opts);
                this.handleJSCommand(jObject);
            } catch (JSONException e) {

            }
        }

        private void handleJSCommand(JSONObject opts) throws JSONException {
            String command = (String) opts.get("command");
            switch (command) {
                case "openPage": {
                    String title = "";
                    if (opts.has("title")) {
                        title = opts.getString("title");
                    }
                    ((KuduActivity) mContext).openContentActivity(opts.getString("url"), title);
                    break;
                }
                case "urlRequest": {
                    handleUrlRequest(opts);
                    break;
                }
                case "addHome": {
                    handleAddHome(opts);
                    break;
                }
                default: {
                    handleTvCommand(opts);
                    break;
                }
            }
        }

        private void handleAddHome(final JSONObject command) throws JSONException {
            if (command.has("id") == false || command.has("url") == false || command.has("name") == false || command.has("icon") == false) {
                return;
            }
            if (command.getString("id").length() < 10) return;

            new AlertDialog.Builder(mContext)
                    .setTitle(null)
                    .setMessage(getString(R.string.confirm_add_home))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            try {
                                Firebase appRef = new Firebase(getString(R.string.firebase_url) + "/apps/" + deviceId + "/installed/" + command.getString("id"));
                                appRef.setValue(toMap(command));
                                String toast = String.format(getString(R.string.added_to_home), command.getString("name"));
                                Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        }

        private void handleUrlRequest(JSONObject command) throws JSONException {
            String url = command.getString("url");
            final String callbackId = command.getString("callbackId");


            Request request = new Request.Builder()
                    .url(url)
                    .build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Request request, IOException e) {

                }

                @Override
                public void onResponse(Response response) throws IOException {
                    //if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

//                    Headers responseHeaders = response.headers();
//                    for (int i = 0; i < responseHeaders.size(); i++) {
//                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
//                    }

                    if (response.isSuccessful()) {


                        String body = response.body().string().replaceAll("\n", "");
                        String encoded = URLEncoder.encode(body, "utf-8").replaceAll("\\+", "%20");
                        final String javascript = "javascript:moviepi.onCallback('" + callbackId + "','" + encoded + "');";

                        //final String javascript = "javascript:moviepi.onCallback('" + callbackId + "','{\"test\":\"3\"}');";

                        ContentActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mWebView.loadUrl(javascript);
                            }
                        });


                    }


                }
            });

        }

        private void handleTvCommand(JSONObject command) throws JSONException {
            Firebase commandRef = new Firebase(mContext.getString(R.string.firebase_url) + "/tvCommand/" + ContentActivity.this.deviceId);
            Map<String, String> commandMap = new HashMap<>();

            Iterator<?> keys = command.keys();

            while (keys.hasNext()) {
                String key = (String) keys.next();
                String value = command.get(key).toString();
                if(value.equals("null")) value = null;
                commandMap.put(key, value);
            }
            commandRef.setValue(commandMap);


            // Toast playing video title
            if(commandMap.containsKey("command") && commandMap.containsKey("title")) {
                String commandStr = commandMap.get("command");
                String title = commandMap.get("title");
                if(commandStr.equals("play")){
                    Toast toast = Toast.makeText(mContext, String.format(getString(R.string.playing_x), title), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -200);
                    toast.show();

                    // Open now playing activity
                    Intent i = new Intent(ContentActivity.this, NowPlayingActivity.class);
                    startActivity(i);
                } else if(commandStr.equals("addToLibrary")){
                    Toast toast = Toast.makeText(mContext, String.format(getString(R.string.x_added_to_library), title), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, -200);
                    toast.show();
                }
            }
        }

    }

}
