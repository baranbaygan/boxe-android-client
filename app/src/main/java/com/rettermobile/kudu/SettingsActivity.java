package com.rettermobile.kudu;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;


import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
@SuppressWarnings("ALL")
public class SettingsActivity extends PreferenceActivity {
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */
    private static final boolean ALWAYS_SIMPLE_PREFS = true;

    private String mDeviceId = "";

    private Firebase deviceConnectionRef;
    private String currentBoxeVersion = "";
    private String latestBoxeVersion = "";

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        setupSimplePreferencesScreen();

        updateConnectionState("Not Connected");
    }

    private void updateConnectionState(final String state) {

        Preference pref = findPreference(getString(R.string.key_connection_state));
        pref.setSummary(state);

        if (state.equals("Connected")) {
            checkForVersion();
        } else {
            final Preference boxeVersionPref = findPreference(getString(R.string.key_kudu_version));
            boxeVersionPref.setTitle(state);
            boxeVersionPref.setSummary(null);
        }
    }

    /**
     * Shows the simplified settings UI if the device configuration if the
     * device configuration dictates that a simplified, single-pane UI should be
     * shown.
     */
    private void setupSimplePreferencesScreen() {
        if (!isSimplePreferences(this)) {
            return;
        }

        // In the simplified UI, fragments are not used at all and we instead
        // use the older PreferenceActivity APIs.

        // Add 'general' preferences.
        addPreferencesFromResource(R.xml.pref_general);

        Preference boxeVersionPref = findPreference(getString(R.string.key_kudu_version));
        boxeVersionPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                if (currentBoxeVersion.equals(latestBoxeVersion) == false) {

                    new AlertDialog.Builder(SettingsActivity.this)
                            .setTitle(null)
                            .setMessage(getString(R.string.confirm_update))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                    // Update BOXE
                                    Firebase commandRef = new Firebase(getString(R.string.firebase_url) + "/tvCommand/" + mDeviceId);
                                    Map<String, String> cmd = new HashMap<String, String>();
                                    cmd.put("command", "update");
                                    commandRef.setValue(cmd);

                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();


                } else {
                    // Check for new version
                    checkForVersion();
                }

                return false;
            }
        });


        Preference boxeAdvancedPref = findPreference(getString(R.string.advanced_settings));
        boxeAdvancedPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {


                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.KITKAT){
                    // Do something for Kitkat and above versions

                    Intent intent = new Intent(SettingsActivity.this, ContentActivity.class);
                    intent.putExtra("url", "http://kudusettings.s3-website-eu-west-1.amazonaws.com/#");
                    intent.putExtra("title", getString(R.string.advanced_settings));
                    startActivity(intent);

                } else{
                    // do something for phones running an SDK before froyo

                    new AlertDialog.Builder(SettingsActivity.this)
                            .setTitle(null)
                            .setMessage(getString(R.string.advanced_settings_not_supported))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })
                            .show();
                }



                return false;
            }
        });


        Preference historyPref = findPreference(getString(R.string.history));
        historyPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle(null)
                        .setMessage(getString(R.string.confirm_delete_history))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                // Delete History
                                Firebase historyRef = new Firebase(getString(R.string.firebase_url) + "/history/" + mDeviceId);
                                historyRef.setValue(null);

                                Toast.makeText(SettingsActivity.this, getString(R.string.history_deleted), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null).show();

                return false;
            }
        });


        Preference restartPref = findPreference(getString(R.string.restart));
        restartPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle(null)
                        .setMessage(getString(R.string.confirm_restart))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.restart, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                Firebase commandRef = new Firebase(SettingsActivity.this.getString(R.string.firebase_url)
                                        + "/tvCommand/" + mDeviceId);
                                Map<String, String> commandMap = new HashMap<>();
                                commandMap.put("command", "restart");
                                commandRef.setValue(commandMap);

                                Toast.makeText(SettingsActivity.this, getString(R.string.restarting_kudu), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null).show();

                return false;
            }
        });

        Preference shutdownPref = findPreference(getString(R.string.shutdown));
        shutdownPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle(null)
                        .setMessage(getString(R.string.confirm_shutdown))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.shutdown, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                Firebase commandRef = new Firebase(SettingsActivity.this.getString(R.string.firebase_url)
                                        + "/tvCommand/" + mDeviceId);
                                Map<String, String> commandMap = new HashMap<>();
                                commandMap.put("command", "shutdown");
                                commandRef.setValue(commandMap);

                                Toast.makeText(SettingsActivity.this, getString(R.string.shutting_down), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null).show();

                return false;
            }
        });

        // Add 'notifications' preferences, and a corresponding header.
//        PreferenceCategory fakeHeader = new PreferenceCategory(this);
//        fakeHeader.setTitle(R.string.pref_header_notifications);
//        getPreferenceScreen().addPreference(fakeHeader);
//        addPreferencesFromResource(R.xml.pref_device_version);

        // Add 'data and sync' preferences, and a corresponding header.
//        fakeHeader = new PreferenceCategory(this);
//        fakeHeader.setTitle(R.string.pref_header_data_sync);
//        getPreferenceScreen().addPreference(fakeHeader);
//        addPreferencesFromResource(R.xml.pref_data_sync);

        // Bind the summaries of EditText/List/Dialog/Ringtone preferences to
        // their values. When their values change, their summaries are updated
        // to reflect the new value, per the Android Design guidelines.

        bindPreferenceSummaryToValue(findPreference(getString(R.string.key_device_id)));

//        bindPreferenceSummaryToValue(findPreference("example_list"));
//        bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));
//        bindPreferenceSummaryToValue(findPreference("sync_frequency"));
    }

    public void onDeviceIdChanged(String deviceId) {
        Log.i("boxe", "Device Id changed in Settings: " + deviceId);

        updateConnectionState("Connecting");

        mDeviceId = deviceId;

        addFirebaseListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mDeviceId != null && mDeviceId.equals("") == false)
            addFirebaseListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        removeFirebaseListeners();
    }

    private void addFirebaseListeners() {


        if (this.deviceConnectionRef != null) {
            this.deviceConnectionRef.removeEventListener(mDeviceConnectionListener);
        }

        this.deviceConnectionRef = new Firebase(getString(R.string.firebase_url) + "/online/" + mDeviceId);
        this.deviceConnectionRef.addValueEventListener(mDeviceConnectionListener);

    }

    private void removeFirebaseListeners() {
        if(this.deviceConnectionRef != null) {
            this.deviceConnectionRef.removeEventListener(mDeviceConnectionListener);
        }
    }

    private void checkForVersion() {

        // Check version
        final Preference boxeVersionPref = findPreference(getString(R.string.key_kudu_version));

        boxeVersionPref.setSelectable(false);

        if (mDeviceId.equals("")) {
            boxeVersionPref.setTitle(null);
            boxeVersionPref.setSummary(null);
            return;
        }

        boxeVersionPref.setTitle(getString(R.string.checking_for_updates));
        boxeVersionPref.setSummary(null);

        Firebase deviceVersionRef = new Firebase(getString(R.string.firebase_url) + "/version/" + mDeviceId);
        deviceVersionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshotForVersion) {
                Firebase deviceVersionRef = new Firebase(getString(R.string.firebase_url) + "/appVersion/version/");
                deviceVersionRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshotForLatestVersion) {

                        boxeVersionPref.setSelectable(true);
                        if (dataSnapshotForVersion.getValue() != null && dataSnapshotForLatestVersion.getValue() != null) {
                            SettingsActivity.this.latestBoxeVersion = dataSnapshotForLatestVersion.getValue().toString();
                            SettingsActivity.this.currentBoxeVersion = dataSnapshotForVersion.getValue().toString();

                            boxeVersionPref.setTitle(SettingsActivity.this.currentBoxeVersion);

                            if (SettingsActivity.this.latestBoxeVersion.equals(SettingsActivity.this.currentBoxeVersion) == false) {
                                boxeVersionPref.setSummary(String.format(getString(R.string.upgrade_to), SettingsActivity.this.latestBoxeVersion));
                            }

                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private ValueEventListener mDeviceConnectionListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.getValue() != null) {
                updateConnectionState("Connected");
            } else {
                updateConnectionState("Not Connected");
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    };

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this) && !isSimplePreferences(this);
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Determines whether the simplified settings UI should be shown. This is
     * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
     * doesn't have newer APIs like {@link PreferenceFragment}, or the device
     * doesn't have an extra-large screen. In these cases, a single-pane
     * "simplified" settings UI should be shown.
     */
    private static boolean isSimplePreferences(Context context) {
        return ALWAYS_SIMPLE_PREFS
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                || !isXLargeTablet(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        if (!isSimplePreferences(this)) {
            loadHeadersFromResource(R.xml.pref_headers, target);
        }
    }


    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference.getKey().equals(SettingsActivity.this.getString(R.string.key_device_id))) {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
                SettingsActivity.this.onDeviceIdChanged(stringValue);
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class GeneralPreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_general);
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
//            bindPreferenceSummaryToValue(findPreference("example_text"));
//            bindPreferenceSummaryToValue(findPreference("example_list"));
//        }
//    }
//
//    /**
//     * This fragment shows notification preferences only. It is used when the
//     * activity is showing a two-pane settings UI.
//     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class NotificationPreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_device_version);
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
//            bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));
//        }
//    }
//
//    /**
//     * This fragment shows data and sync preferences only. It is used when the
//     * activity is showing a two-pane settings UI.
//     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class DataSyncPreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_data_sync);
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
//            bindPreferenceSummaryToValue(findPreference("sync_frequency"));
//        }
//    }
}
