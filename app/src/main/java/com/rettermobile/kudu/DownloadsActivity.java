package com.rettermobile.kudu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


public class DownloadsActivity extends KuduActivity {

    private ListView listView;
    private DownloadsListAdapter mAdapter;

    private Firebase mDownloadsRef;

    private Firebase mCurrentDownloadRef;
    private Firebase mQueueRef;
    private Firebase mCompletedRef;


    // Firebase Data
    private HashMap<String, Object> mDownloadsData;
    private HashMap<String, Object> mCurrentDownloadData;
    private HashMap<String, Object> mQueueData;
    private HashMap<String, Object> mCompletedData;

    private TextView emptyText;

    private int mSelectedMediaPosition;
    private HashMap<String, Object> mSelectedMedia;

    private ArrayList<HashMap<String, String>> subs = new ArrayList<>();

    private Boolean mSelectSubtitles = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);

        this.emptyText = (TextView) findViewById(R.id.emptyText);

        this.listView = (ListView) findViewById(R.id.listViewDownloads);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSelectedMediaPosition = position;
                mSelectedMedia = (HashMap<String, Object>) mAdapter.getItem(position);
                mSelectSubtitles = false;
                openContextMenu(listView);
            }
        });
        this.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mSelectedMediaPosition = position;
                mSelectedMedia = (HashMap<String, Object>) mAdapter.getItem(position);
                mSelectSubtitles = false;
                return false;
            }
        });

//        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                int itemType = mAdapter.getItemViewType(position);
//                if (itemType == 0) {
//                    // Ask to stop download
//                    new AlertDialog.Builder(DownloadsActivity.this)
//                            .setTitle(null)
//                            .setMessage(getString(R.string.confirm_stop_download))
//                            .setIcon(android.R.drawable.ic_dialog_alert)
//                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int whichButton) {
//
//                                    Firebase commandRef = new Firebase(getString(R.string.firebase_url) + "/tvCommand/" + DownloadsActivity.this.deviceId);
//                                    Map<String, String> commandMap = new HashMap<>();
//                                    commandMap.put("command", "cancelCurrentDownload");
//                                    commandRef.setValue(commandMap);
//
////
//                                }
//                            })
//                            .setNegativeButton(android.R.string.no, null).show();
//                } else {
//                    HashMap<String, Object> item = (HashMap<String, Object>) mAdapter.getItem(position);
//                    final String mediaId = item.get("id").toString();
//                    final String mediaUrl = item.get("url").toString();
//
//                    if(item.get("status").toString().equals(getString(R.string.queued))) {
//                        new AlertDialog.Builder(DownloadsActivity.this)
//                                .setTitle(null)
//                                .setMessage(getString(R.string.confirm_remove_from_queue))
//                                .setIcon(android.R.drawable.ic_dialog_alert)
//                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int whichButton) {
//                                        Firebase commandRef = new Firebase(getString(R.string.firebase_url) + "/tvCommand/" + DownloadsActivity.this.deviceId);
//                                        Map<String, String> commandMap = new HashMap<>();
//                                        commandMap.put("command", "removeFromQueue");
//                                        commandMap.put("url", mediaUrl);
//                                        commandRef.setValue(commandMap);
//                                    }
//                                })
//                                .setNegativeButton(android.R.string.no, null).show();
//                    } else {
//                        new AlertDialog.Builder(DownloadsActivity.this)
//                                .setTitle(null)
//                                .setMessage(getString(R.string.confirm_delete_media))
//                                .setIcon(android.R.drawable.ic_dialog_alert)
//                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int whichButton) {
//                                        Firebase commandRef = new Firebase(getString(R.string.firebase_url) + "/tvCommand/" + DownloadsActivity.this.deviceId);
//                                        Map<String, String> commandMap = new HashMap<>();
//                                        commandMap.put("command", "deleteMedia");
//                                        commandMap.put("url", mediaUrl);
//                                        commandRef.setValue(commandMap);
//                                    }
//                                })
//                                .setNegativeButton(android.R.string.no, null).show();
//                    }
//                }
//            }
//        });


        registerForContextMenu(listView);

        subs = new ArrayList<>();

        HashMap<String, String> english = new HashMap<String, String>();
        english.put("name", "English");
        english.put("code", "eng");
        subs.add(english);

        HashMap<String, String> turkish = new HashMap<String, String>();
        turkish.put("name", "Turkish");
        turkish.put("code", "tur");
        subs.add(turkish);

        HashMap<String, String> french = new HashMap<String, String>();
        french.put("name", "French");
        french.put("code", "fre");
        subs.add(french);

        HashMap<String, String> italien = new HashMap<String, String>();
        italien.put("name", "Italien");
        italien.put("code", "ita");
        subs.add(italien);

        HashMap<String, String> dutch = new HashMap<String, String>();
        dutch.put("name", "Dutch");
        dutch.put("code", "dut");
        subs.add(dutch);

//        HashMap<String, String> german = new HashMap<String, String>();
//        german.put("name", "German");
//        german.put("code", "deu");
//        subs.add(german);

        HashMap<String, String> czech = new HashMap<String, String>();
        czech.put("name", "Czech");
        czech.put("code", "cze");
        subs.add(czech);

        HashMap<String, String> spanish = new HashMap<String, String>();
        spanish.put("name", "Spanish");
        spanish.put("code", "spa");
        subs.add(spanish);

        HashMap<String, String> swedish = new HashMap<String, String>();
        swedish.put("name", "Swedish");
        swedish.put("code", "swe");
        subs.add(swedish);

        HashMap<String, String> portuguese = new HashMap<String, String>();
        portuguese.put("name", "Portuguese");
        portuguese.put("code", "por");
        subs.add(portuguese);


//        HashMap<String, String> hindustani = new HashMap<String, String>();
//        hindustani.put("name", "Hindustani");
//        hindustani.put("code", "hin");
//        subs.add(hindustani);
//
//        HashMap<String, String> arabic = new HashMap<String, String>();
//        arabic.put("name", "Arabic");
//        arabic.put("code", "ara");
//        subs.add(arabic);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        if (mSelectedMedia != null && mSelectSubtitles) {
            // Show subtitles menu
            menu.setHeaderTitle(getResources().getString(R.string.subtitles));

            for (HashMap<String, String> lang : subs) {
                menu.add(0, subs.indexOf(lang), subs.indexOf(lang), lang.get("name"));
            }

        } else {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_download_item, menu);
        }


        super.onCreateContextMenu(menu, v, menuInfo);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {


        if (mSelectedMedia != null && mSelectSubtitles == false) {

            final String mediaId = mSelectedMedia.get("id").toString();
            final String mediaUrl = mSelectedMedia.get("url").toString();

            // Media menu item selected
            switch (item.getItemId()) {
                case R.id.action_play_media: {
                    playMedia(null);
                    break;
                }
                case R.id.action_media_details: {
                    Intent intent = new Intent(DownloadsActivity.this, ContentActivity.class);
                    intent.putExtra("url", (String) mSelectedMedia.get("containerUrl"));
                    intent.putExtra("title", (String) mSelectedMedia.get("title"));
                    DownloadsActivity.this.startActivity(intent);
                    break;
                }
                case R.id.action_play_media_with_subs: {
                    mSelectSubtitles = true;
                    listView.post(new Runnable() {
                        @Override
                        public void run() {
                            listView.showContextMenu();
                        }
                    });
                    //openContextMenu(listView);
                    return true;

                }
                case R.id.action_remove_media: {
                    int viewType = mAdapter.getItemViewType(mSelectedMediaPosition);
                    if (viewType == 0) {
                        // Ask to stop download
                        new AlertDialog.Builder(DownloadsActivity.this)
                                .setTitle(null)
                                .setMessage(getString(R.string.confirm_stop_download))
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        Firebase commandRef = new Firebase(getString(R.string.firebase_url) + "/tvCommand/" + DownloadsActivity.this.deviceId);
                                        Map<String, String> commandMap = new HashMap<>();
                                        commandMap.put("command", "cancelCurrentDownload");
                                        commandRef.setValue(commandMap);

//
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null).show();

                    } else {
                        new AlertDialog.Builder(DownloadsActivity.this)
                                .setTitle(null)
                                .setMessage(getString(R.string.confirm_delete_media))
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        Firebase nowPlayingMediaRef = new Firebase(getString(R.string.firebase_url)
                                                + "/nowplayingmedia/" + DownloadsActivity.this.deviceId + "/url");
                                        nowPlayingMediaRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {

                                                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                                                    String url = dataSnapshot.getValue().toString();
                                                    if (url.equals(mediaUrl)) {
                                                        new AlertDialog.Builder(DownloadsActivity.this)
                                                                .setTitle(null)
                                                                .setMessage(getString(R.string.cant_delete_playing_video))
                                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                    }
                                                                })
                                                                .show();
                                                    } else {
                                                        sendDeleteMediaCommand(mediaUrl);
                                                    }
                                                } else {
                                                    sendDeleteMediaCommand(mediaUrl);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(FirebaseError firebaseError) {

                                            }
                                        });


                                    }
                                })
                                .setNegativeButton(android.R.string.no, null).show();
                    }


                    break;
                }
            }

        } else {
            // Subtitle selected
            playMedia(subs.get(item.getOrder()));

        }

        return super.onContextItemSelected(item);
    }

    private void sendDeleteMediaCommand(String mediaUrl) {
        Firebase commandRef = new Firebase(getString(R.string.firebase_url) + "/tvCommand/" + DownloadsActivity.this.deviceId);
        Map<String, String> commandMap = new HashMap<>();
        commandMap.put("command", "deleteMedia");
        commandMap.put("url", mediaUrl);
        commandRef.setValue(commandMap);
    }

    private void playMedia(HashMap<String, String> selectedSub) {
        Firebase commandRef = new Firebase(this.getString(R.string.firebase_url) + "/tvCommand/" + this.deviceId);
        Map<String, String> commandMap = new HashMap<>();

        String path = "";
        if(mSelectedMedia.containsKey("path") && mSelectedMedia.containsKey("files")) {
            path = mSelectedMedia.get("path").toString();
            ArrayList<HashMap<String, Object>> files = (ArrayList) mSelectedMedia.get("files");
            for (HashMap<String, Object> file : files) {
                if (file.get("name").toString().endsWith(".mp4") ||
                        file.get("name").toString().endsWith(".avi") ||
                        file.get("name").toString().endsWith(".mkv")) {
                    path = path + "/" + file.get("path").toString();
                    break;
                }
            }
        }



        commandMap.put("command", "play");
        commandMap.put("url", mSelectedMedia.get("url").toString());
        commandMap.put("filePath", path);
        commandMap.put("mediaType", mSelectedMedia.get("mediaType").toString());
        commandMap.put("icon", mSelectedMedia.get("icon").toString());
        commandMap.put("name", mSelectedMedia.get("title").toString());
        commandMap.put("containerUrl", mSelectedMedia.get("containerUrl").toString());
        commandMap.put("history", "false");
        commandMap.put("title", mSelectedMedia.get("title").toString());

        if (selectedSub != null) {
            commandMap.put("subtitleLangCode", selectedSub.get("code").toString());
        }


        commandRef.setValue(commandMap);


        // Toast playing video title
        if (commandMap.containsKey("command") && commandMap.containsKey("title")) {
            String commandStr = commandMap.get("command");
            String title = commandMap.get("title");
            if (commandStr.indexOf("play") != -1) { //.equals("play")) {
                Toast toast = Toast.makeText(this, String.format(getString(R.string.playing_x), title), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, -200);
                toast.show();

                // Open now playing activity
                Intent i = new Intent(DownloadsActivity.this, NowPlayingActivity.class);
                startActivity(i);
            }
//            else if (commandStr.indexOf("addToLibrary") != -1) {
//                Toast toast = Toast.makeText(this, String.format(getString(R.string.x_added_to_library), title), Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.CENTER, 0, -200);
//                toast.show();
//            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (this.hasMenu() == false) return true;

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_downloads, menu);

        nowPlayingMenuItem = menu.findItem(R.id.now_playing);
        nowPlayingMenuItem.setVisible(false);

        listenNowPlayingMedia();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id = item.getItemId();

        if (id == R.id.action_clear_queue) {
            new AlertDialog.Builder(DownloadsActivity.this)
                    .setTitle(null)
                    .setMessage(getString(R.string.confirm_clear_queue))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            Firebase commandRef = new Firebase(getString(R.string.firebase_url) + "/tvCommand/" + DownloadsActivity.this.deviceId);
                            Map<String, String> commandMap = new HashMap<>();
                            commandMap.put("command", "clearDownloadQueue");
                            commandRef.setValue(commandMap);

                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.removeValueEventListeners();
    }

    private void removeValueEventListeners() {
        if (mDownloadsRef != null) {
            mDownloadsRef.removeEventListener(mDownloadsListener);
        }
        if (mCurrentDownloadRef != null) {
            mCurrentDownloadRef.removeEventListener(mCurrentDownloadListener);
        }
        if (mQueueRef != null) {
            mQueueRef.removeEventListener(mQueueListener);
        }
        if (mCompletedRef != null) {
            mCompletedRef.removeEventListener(mCompletedListener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.removeValueEventListeners();

        if (this.deviceId.equals("") == false) {
//            mDownloadsRef = new Firebase(getString(R.string.firebase_url) + "/download/" + this.deviceId);
//            mDownloadsRef.addValueEventListener(mDownloadsListener);

            mCurrentDownloadRef = new Firebase(getString(R.string.firebase_url) + "/download/" + this.deviceId + "/downloading");
            mCurrentDownloadRef.addValueEventListener(mCurrentDownloadListener);

            mQueueRef = new Firebase(getString(R.string.firebase_url) + "/download/" + this.deviceId + "/queue");
            mQueueRef.addValueEventListener(mQueueListener);

            mCompletedRef = new Firebase(getString(R.string.firebase_url) + "/download/" + this.deviceId + "/completed");
            mCompletedRef.addValueEventListener(mCompletedListener);
        }
    }


    private DownloadsListener mDownloadsListener = new DownloadsListener();

    private class DownloadsListener implements ValueEventListener {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            Log.i("kudu", "DownloadsListener onDataChange");

            DownloadsActivity.this.mDownloadsData = (HashMap<String, Object>) dataSnapshot.getValue();
            DownloadsActivity.this.buildData();
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    }

    private CurrentDownloadListener mCurrentDownloadListener = new CurrentDownloadListener();

    private class CurrentDownloadListener implements ValueEventListener {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            HashMap<String, Object> currentDownloadData = (HashMap<String, Object>) dataSnapshot.getValue();

            // Check if a download is just starting or ended
            if ((currentDownloadData != null && DownloadsActivity.this.mCurrentDownloadData == null) ||
                    (currentDownloadData == null && DownloadsActivity.this.mCurrentDownloadData != null)) {
                DownloadsActivity.this.mCurrentDownloadData = currentDownloadData;
                DownloadsActivity.this.buildData();
            } else {
                DownloadsActivity.this.mCurrentDownloadData = currentDownloadData;
            }


            // Update progress bar for current download
            if (dataSnapshot.getValue() != null && mAdapter != null && mAdapter.getItemViewType(0) == 0) {
                View view = listView.getChildAt(0);
                if (view != null && view.getTag() instanceof DownloadsListAdapter.ProgressListItemViewHolder) {
                    DownloadsListAdapter.ProgressListItemViewHolder viewHolder = (DownloadsListAdapter.ProgressListItemViewHolder) view.getTag();
                    HashMap<String, Object> downloadingData = (HashMap<String, Object>) dataSnapshot.getValue();
                    Double total = Double.parseDouble(downloadingData.get("total").toString());
                    Double downloaded = Double.parseDouble(downloadingData.get("downloaded").toString());
                    int progress = (int) (100 * downloaded / total);
                    //double speed = Double.parseDouble(downloadingData.get("speed").toString()) / 1000000;

                    viewHolder.progressBar.setProgress(progress);
                    viewHolder.progressText.setText("%" + progress + " - " + downloadingData.get("speed").toString());
                }
            }
        }

        public double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            long factor = (long) Math.pow(10, places);
            value = value * factor;
            long tmp = Math.round(value);
            return (double) tmp / factor;
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    }

    private QueueListener mQueueListener = new QueueListener();

    private class QueueListener implements ValueEventListener {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            Log.i("kudu", "QueueListener onDataChange");

            DownloadsActivity.this.mQueueData = (HashMap<String, Object>) dataSnapshot.getValue();
            DownloadsActivity.this.buildData();
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    }

    private CompletedListener mCompletedListener = new CompletedListener();

    private class CompletedListener implements ValueEventListener {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            Log.i("kudu", "CompletedListener onDataChange");

            DownloadsActivity.this.mCompletedData = (HashMap<String, Object>) dataSnapshot.getValue();
            DownloadsActivity.this.buildData();
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    }

    private void buildData() {
        ArrayList<HashMap<String, Object>> items = new ArrayList<>();

        Boolean showProgress = false;

        if (mCurrentDownloadData != null) {
            items.add(mCurrentDownloadData);
            showProgress = true;
        }

        if (mCompletedData != null) {
            ArrayList<HashMap<String, Object>> completedItems = new ArrayList<>();
            Object[] keys = mCompletedData.keySet().toArray();
            for (Object key : keys) {
                HashMap<String, Object> obj = (HashMap<String, Object>) mCompletedData.get(key);
                obj.put("status", getString(R.string.completed));
                completedItems.add(obj);
            }

            Collections.sort(completedItems, new Comparator<HashMap<String, Object>>() {
                @Override
                public int compare(HashMap<String, Object> lhs, HashMap<String, Object> rhs) {
                    return lhs.get("name").toString().compareTo(rhs.get("name").toString());
                }
            });

            items.addAll(completedItems);
        }

        if (mQueueData != null) {

            Object[] keys = mQueueData.keySet().toArray();
            for (Object key : keys) {

                HashMap<String, Object> obj = (HashMap<String, Object>) mQueueData.get(key);

                // If this item is currently downloading dont add it to queued list
                if (mCurrentDownloadData != null) {
                    if (mCurrentDownloadData.get("id").toString().equals(obj.get("id").toString())) {
                        continue;
                    }
                }

                obj.put("status", getString(R.string.queued));
                items.add(obj);
            }

        }

        if (items.size() == 0) {
            this.emptyText.setVisibility(View.VISIBLE);
            this.listView.setVisibility(View.GONE);
        } else {
            this.emptyText.setVisibility(View.GONE);
            this.listView.setVisibility(View.VISIBLE);
        }

        mAdapter = new DownloadsListAdapter(this, items, showProgress);
        listView.setAdapter(mAdapter);


    }

    // Adapter

    public class DownloadsListAdapter extends BaseAdapter {

        public ArrayList<HashMap<String, Object>> mItems;
        public Boolean mShowProgress;
        Context mContext;

        public DownloadsListAdapter(Context context, ArrayList<HashMap<String, Object>> items, Boolean showProgress) {


            this.mItems = items;
            this.mContext = context;
            this.mShowProgress = showProgress;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public int getCount() {
            return this.mItems == null ? 0 : this.mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return this.mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        private class ProgressListItemViewHolder {
            public ProgressBar progressBar;
            public TextView nameText;
            public TextView progressText;
            public ImageView mediaImage;
        }

        private class ListItemViewHolder {
            public ImageView mediaImage;
            public ImageView statusImage;
            public TextView nameText;
            public TextView statusText;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            int viewType = getItemViewType(position);
            if (viewType == 0) {

                ProgressListItemViewHolder viewHolder = null;
                // Progress item
                if (convertView != null) {
                    viewHolder = (ProgressListItemViewHolder) convertView.getTag();
                } else {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(R.layout.downloading_progress_list_item, parent, false);
                    viewHolder = new ProgressListItemViewHolder();
                    viewHolder.nameText = (TextView) convertView.findViewById(R.id.nameText);
                    viewHolder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
                    viewHolder.progressText = (TextView) convertView.findViewById(R.id.progressText);
                    viewHolder.mediaImage = (ImageView) convertView.findViewById(R.id.mediaImage);
                    convertView.setTag(viewHolder);
                }

                HashMap<String, Object> item = (HashMap<String, Object>) getItem(position);

                if (item.get("title") != null) {
                    viewHolder.nameText.setText((String) item.get("title"));
                }

                if (item.get("icon") != null) {
                    Picasso.with(mContext).load((String) item.get("icon")).into(viewHolder.mediaImage);
                }

                return convertView;

            } else {
                ListItemViewHolder viewHolder = null;
                // List item (Completed & Queued)
                if (convertView != null) {
                    viewHolder = (ListItemViewHolder) convertView.getTag();
                } else {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(R.layout.downloads_list_item, parent, false);
                    viewHolder = new ListItemViewHolder();
                    viewHolder.nameText = (TextView) convertView.findViewById(R.id.nameText);
                    viewHolder.mediaImage = (ImageView) convertView.findViewById(R.id.mediaImage);
                    viewHolder.statusText = (TextView) convertView.findViewById(R.id.statusText);
                    viewHolder.statusImage = (ImageView) convertView.findViewById(R.id.statusImage);
                    convertView.setTag(viewHolder);
                }

                HashMap<String, Object> item = (HashMap<String, Object>) getItem(position);

                if (item.get("title") != null) {
                    viewHolder.nameText.setText((String) item.get("title"));
                }
                if (item.get("icon") != null) {
                    Picasso.with(mContext).load((String) item.get("icon")).into(viewHolder.mediaImage);
                }
                String status = (String) item.get("status");
                viewHolder.statusText.setText(status);

                viewHolder.statusText.setVisibility(View.VISIBLE);

                if (status.equals(getString(R.string.completed))) {

                    viewHolder.statusText.setVisibility(View.GONE);
                    convertView.setBackground(getResources().getDrawable(R.drawable.download_list_item_background));

                } else {
                    convertView.setBackground(getResources().getDrawable(R.drawable.download_queued_list_item_background));
                    //convertView.setBackground(getResources().getDrawable(R.drawable.download_queued_list_item_background));
                }


                return convertView;
            }

        }

        @Override
        public int getItemViewType(int position) {
            if (mItems == null) return 1;
            if (mShowProgress && position == 0) {
                return 0;
            } else {
                return 1;
            }

        }

        @Override
        public int getViewTypeCount() {
            return mShowProgress ? 2 : 1;
        }
    }

}
