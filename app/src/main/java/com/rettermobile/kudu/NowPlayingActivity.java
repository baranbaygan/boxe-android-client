package com.rettermobile.kudu;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.HashMap;
import java.util.Map;


public class NowPlayingActivity extends KuduActivity {

    private TextView txtMediaName;
    private Button btnPlayPause;
    private Button btnStop;
    private Button btnForward;
    private Button btnBackward;
    private TextView txtPosition;
    private SeekBar seekBar;
    private View nothingPlaying;
    private Boolean seekBarDragging = false;

    private Firebase commandRef;

    private Double position;
    private Double duration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_now_playing);

        Log.i("boxe", "onCreate");

        this.txtMediaName = (TextView) findViewById(R.id.txt_media_name);
        this.txtPosition = (TextView) findViewById(R.id.txt_position);
        this.btnPlayPause = (Button) findViewById(R.id.btn_playPause);
        this.btnStop = (Button) findViewById(R.id.btn_stop);
        this.btnBackward = (Button) findViewById(R.id.btn_rewind);
        this.btnForward = (Button) findViewById(R.id.btn_forward);
        this.seekBar = (SeekBar) findViewById(R.id.seek_bar);
        this.nothingPlaying = findViewById(R.id.nothingPlaying);


        this.btnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> command = new HashMap<String, String>();
                command.put("command", "playPause");
                commandRef.setValue(command);
            }
        });

        this.btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> command = new HashMap<String, String>();
                command.put("command", "stop");
                commandRef.setValue(command);

                NowPlayingActivity.this.finish();
            }
        });

        this.btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> command = new HashMap<String, String>();
                command.put("command", "seek");
                command.put("offset", "30");
                commandRef.setValue(command);
            }
        });

        this.btnBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> command = new HashMap<String, String>();
                command.put("command", "seek");
                command.put("offset", "-30");
                commandRef.setValue(command);
            }
        });

        this.seekBar.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                return false;
            }
        });
        this.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (seekBar != null && duration != null) {

                    //int progress = seekBar.getProgress();
                    double targetPosition = Double.valueOf(progress) / 100 * duration;

                    NowPlayingActivity.this.txtPosition.setText(minuteStringFromDouble(targetPosition) + " / " +
                            minuteStringFromDouble(NowPlayingActivity.this.duration));
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarDragging = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBarDragging = false;

                if (commandRef != null) {
                    double targetPosition = Double.valueOf(seekBar.getProgress()) / 100 * duration;
                    double offset = (targetPosition - position) / 1000000;
                    Map<String, String> command = new HashMap<String, String>();
                    command.put("command", "seek");
                    String val = String.format("%.0f", offset);
                    command.put("offset", val);
                    commandRef.setValue(command);
                }
            }
        });

        enableOrDisableControls(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i("boxe", "onDestroy");
        this.seekBar.setOnSeekBarChangeListener(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (this.deviceId.equals("") == false) {

            this.commandRef = new Firebase(getString(R.string.firebase_url) + "/tvCommand/" + this.deviceId);

            Firebase nowPlayingRef = new Firebase(getString(R.string.firebase_url) + "/nowplaying/" + this.deviceId);
            nowPlayingRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    HashMap<String, Object> nowplayingInfo = (HashMap<String, Object>) dataSnapshot.getValue();
                    if (nowplayingInfo != null) {

                        NowPlayingActivity.this.position = Double.valueOf((String) nowplayingInfo.get("position"));
                        NowPlayingActivity.this.duration = Double.valueOf((String) nowplayingInfo.get("duration"));


                        if (seekBarDragging == false) {
                            seekBar.setProgress((int) (position / duration * 100));

                            NowPlayingActivity.this.txtPosition.setText(minuteStringFromDouble(NowPlayingActivity.this.position) + " / " +
                                    minuteStringFromDouble(NowPlayingActivity.this.duration));
                        }

                        if (nowplayingInfo.get("paused").toString().equals("true")) {
                            btnPlayPause.setBackground(getResources().getDrawable(R.drawable.play));
                        } else {
                            btnPlayPause.setBackground(getResources().getDrawable(R.drawable.pause));
                        }

                        // Enable controls
                        enableOrDisableControls(true);

                    } else {
                        //NowPlayingActivity.this.nothingPlaying.setVisibility(View.GONE);
                        // Disable controls
                        enableOrDisableControls(false);
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
            Firebase nowPlayingMediaRef = new Firebase(getString(R.string.firebase_url) + "/nowplayingmedia/" + this.deviceId);
            nowPlayingMediaRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    HashMap<String, Object> media = (HashMap<String, Object>) dataSnapshot.getValue();
                    if (media != null) {
                        NowPlayingActivity.this.txtMediaName.setText((String) media.get("title"));
                        NowPlayingActivity.this.nothingPlaying.setVisibility(View.GONE);
                    } else {
                        NowPlayingActivity.this.nothingPlaying.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        } else {
            NowPlayingActivity.this.nothingPlaying.setVisibility(View.VISIBLE);
        }

    }

    private void enableOrDisableControls(Boolean enabled) {
        this.txtMediaName.setEnabled(enabled);
        this.btnPlayPause.setEnabled(enabled);
        this.btnForward.setEnabled(enabled);
        this.btnBackward.setEnabled(enabled);
        this.seekBar.setEnabled(enabled);
        this.txtPosition.setEnabled(enabled);
    }

    private String minuteStringFromDouble(Double val) {
        int totalSecs = (int) (val / 1000000);
        int mins = 0;
        if (totalSecs >= 60) {
            mins = (totalSecs - (totalSecs % 60)) / 60;
        } else {
            mins = 0;
        }

        int secs = totalSecs % 60;

        String minsStr = String.valueOf(mins);
        String secStr = String.valueOf(secs);
        if (minsStr.length() == 1) {
            minsStr = "0" + minsStr;
        }
        if (secStr.length() == 1) {
            secStr = "0" + secStr;
        }

        return minsStr + ":" + secStr;
    }

//    func minuteStringFromDouble(val:Double) -> String {
//
//        var totalSecs = Int(val / 1000000)
//        var mins = 0;
//        if(totalSecs>=60) {
//            mins = (totalSecs - (totalSecs % 60)) / 60
//        } else {
//            mins = 0
//        }
//
//        var secs = totalSecs % 60
//
//        var minsStr:String = "\(mins)"
//        var secStr = "\(secs)"
//
//        if(count(minsStr)==1) {
//            minsStr = "0" + minsStr
//        }
//        if(count(secStr)==1) {
//            secStr = "0" + secStr
//        }
//
//        return "\(minsStr):\(secStr)"
//    }


    @Override
    protected Boolean hasMenu() {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_now_playing, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_media_details) {

            Firebase nowPlayingMediaRef = new Firebase(getString(R.string.firebase_url) + "/nowplayingmedia/" + this.deviceId);
            nowPlayingMediaRef.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap<String, Object> media = (HashMap<String, Object>) dataSnapshot.getValue();
                    if (media != null) {
                        Intent intent = new Intent(NowPlayingActivity.this, ContentActivity.class);
                        intent.putExtra("title", (String) media.get("title"));
                        intent.putExtra("url", (String) media.get("containerUrl"));
                        NowPlayingActivity.this.startActivity(intent);
                        NowPlayingActivity.this.finish();
                    }

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
